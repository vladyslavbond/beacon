Beacon = {}

local BEACON_OF_LIGHT = 53563
local BEACON_OF_LIGHT_SPELL_HEAL = 53654

local function applySpellCastSuccess(self, ...)
	assert(self ~= nil)

	--[[ See OnUpdate update processor. ]]--
	self.lastSpellCastSuccess = GetTime()
end

local function acceptSpellCastSuccess(self, ...)
	assert(self ~= nil)

	local e = select(2, ...)
	local casterGUID = select(3, ...)
	local spellId = select(9, ...)

	if 'SPELL_CAST_SUCCESS' == e and UnitGUID('player') == casterGUID and BEACON_OF_LIGHT == spellId then
		applySpellCastSuccess(self, ...)
	end
end

local function applySpellCastFailed(self, ...)
	assert(self ~= nil)

	--[[ See OnUpdate update processor. ]]--
	self.lastSpellCastFailed = GetTime()
end

local function acceptSpellCastFailed(self, ...)
	assert(self ~= nil)

	local e = select(2, ...)
	local casterGUID = select(3, ...)
	local spellId = select(9, ...)

	if 'SPELL_CAST_FAILED' == e and UnitGUID('player') == casterGUID and BEACON_OF_LIGHT == spellId then
		applySpellCastFailed(self, ...)
	end
end

local function applySpellHeal(self, ...)
	assert(self ~= nil)

	local footerLabel = _G[string.format('%sText1', self:GetName())]
	assert(footerLabel ~= nil)

	local spellId = select(9, ...)
	if BEACON_OF_LIGHT_SPELL_HEAL == spellId then
		local healAmount = select(12, ...) or 0
		local overhealAmount = select(13, ...) or 0
		local t = string.format('%.f', healAmount - overhealAmount)
		footerLabel:SetText(healAmount - overhealAmount)
		footerLabel:SetTextColor(0, 1, 0)
	else
		footerLabel:SetText(0)
		footerLabel:SetTextColor(1, 1, 1)
	end
	--[[ See OnUpdate update processor. ]]--
	self.lastSpellHeal = GetTime()
end

local function acceptSpellHeal(self, ...)
	assert(self ~= nil)

	local e = select(2, ...)
	local casterGUID = select(3, ...)

	if 'SPELL_HEAL' == e and UnitGUID('player') == casterGUID then
		applySpellHeal(self, ...)
	end
end

local function applyBeacon(self, unitDesignation, ...)
	local cache = BeaconCache
	assert(cache ~= nil)
	assert('table' == type(cache))

	cache.unit = unitDesignation
	cache.unitGUID = UnitGUID(unitDesignation)
	cache.spell = select(1, ...)
	cache.spellId = select(11, ...)
	local durationSec = select(6, ...)
	local expirationInstance = select(7, ...)
	cache.expirationInstance = expirationInstance

	local artwork = _G[string.format('%sArtwork', self:GetName())]
	assert(artwork ~= nil)

	local headerLabel = _G[string.format('%sText3', self:GetName())]
	assert(headerLabel ~= nil)
	headerLabel:SetText(UnitName(unitDesignation))

	local durationLabel = _G[string.format('%sText2', self:GetName())]
	assert(durationLabel ~= nil)
	durationLabel:SetText(durationSec)

	local footerLabel = _G[string.format('%sText1', self:GetName())]
	assert(footerLabel ~= nil)

	self.lastSpellHeal = self.lastSpellHeal or 0
	self.lastSpellCastFailed = self.lastSpellCastFailed or 0
	self.lastSpellCastSuccess = self.lastSpellCastSuccess or 0

	self:SetScript('OnUpdate', function(f)
		assert(f ~= nil)

		local now = GetTime()
		local durationRemaining = expirationInstance - now
		local t = string.format('%.f', durationRemaining)
		durationLabel:SetText(t)

		if UnitExists(cache.unit) and UnitInRange(cache.unit, cache.spell) and durationRemaining > 0 then
			artwork:SetAlpha(1)
		else
			self:Hide()
			artwork:SetAlpha(0.4)
		end

		if f.lastSpellHeal then
			local a = math.min(math.max(0, (f.lastSpellHeal + 4.0) - now), 1)
			footerLabel:SetAlpha(a)
		end

		if f.lastSpellCastFailed > f.lastSpellCastSuccess then
			local b = 0
			artwork:SetVertexColor(1, b, b)
		else
			artwork:SetVertexColor(1, 1, 1)
		end
	end)
	self:Show()
end

local function acceptUnitAura(self, unitDesignation)
	assert(self ~= nil)

	assert(unitDesignation ~= nil)
	assert('string' == type(unitDesignation))
	unitDesignation = string.lower(strtrim(unitDesignation))
	assert(string.len(unitDesignation) >= 1)
	assert(string.len(unitDesignation) <= 65536)

	if not UnitExists(unitDesignation) or (not UnitInRaid(unitDesignation) and not UnitInParty(unitDesignation)) then
		return false
	end

	local i = 0
	while(i < 8192) do
		i = i + 1
		local record = {UnitAura(unitDesignation, i, 'PLAYER HELPFUL')}
		local auraName = record[1]
		local auraId = record[11]
		if nil == auraName then
			break
		end
		if BEACON_OF_LIGHT == auraId then
			applyBeacon(self, unitDesignation, unpack(record))
		end
	end
end

local function eventProcessor(self, eventCategory, ...)
	assert(self ~= nil)

	assert(eventCategory ~= nil)
	assert('string' == type(eventCategory))
	eventCategory = string.upper(strtrim(eventCategory))
	assert(string.len(eventCategory) >= 1)
	assert(string.len(eventCategory) <= 65536)

	if 'UNIT_AURA' == eventCategory then
		local unitDesignation = select(1, ...)
		assert(unitDesignation ~= nil)
		assert('string' == type(unitDesignation))

		if UnitExists(unitDesignation) and (UnitInRaid(unitDesignation) or UnitInParty(unitDesignation)) then
			acceptUnitAura(self, unitDesignation)
		end
	elseif 'COMBAT_LOG_EVENT_UNFILTERED' == eventCategory then
		acceptSpellHeal(self, ...)
		acceptSpellCastFailed(self, ...)
		acceptSpellCastSuccess(self, ...)
	end
end

local function init(self)
	assert(self ~= nil)

	self:UnregisterEvent('PLAYER_LOGIN')

	if not BeaconCache then
		BeaconCache = {}
	end

	self:RegisterEvent('UNIT_AURA')
	self:RegisterEvent('COMBAT_LOG_EVENT_UNFILTERED')
	self:SetScript('OnEvent', eventProcessor)
end

function Beacon.main(self)
	assert(self ~= nil)

	self:RegisterEvent('PLAYER_LOGIN')
	self:SetScript('OnEvent', init)
end
